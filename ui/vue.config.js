const host = 'localhost';
const port = 4000;

module.exports = {
    transpileDependencies: [
        'vuetify'
    ],
    devServer: {
        port,
        host,
        hot: true,
        disableHostCheck: true,
        clientLogLevel: 'warning',
        inline: true,
        proxy: {
            '^/slp': {
                target: 'http://localhost:8086/',
                ws: true,
                changeOrigin: true
            }
        }
    },
    lintOnSave: false
}
