import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        authCode: ''
    },
    mutations: {
        changeAuthCode(state, value) {
            state.authCode = value;
        }
    },
    actions: {

    },
    getters: {
        authCode: state => state.authCode
    }
});

