export const PATH_VARIABLES = {
  slp: 'slp',
  spotify: 'spotify'
}

export const SPOTIFY_PARAMS = {
  clientId: '2313ba015a6e4262827ec703612e3d6e',
  clientSecret: 'a85cc550ef094afca9d67beb7a92f794'
};

export const SPOTIFY_URLS = {
  authorize: 'https://accounts.spotify.com/authorize'
}
