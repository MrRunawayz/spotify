import {PATH_VARIABLES} from "./constants";
import Vue from "vue";

export function getAccessToken() {
    return `${Vue.prototype.$backendUrl}/${PATH_VARIABLES.slp}/${PATH_VARIABLES.spotify}/get_access_token`;
}

export function getSpotifyTrackInfo() {
    return `${Vue.prototype.$backendUrl}/${PATH_VARIABLES.slp}/${PATH_VARIABLES.spotify}/get_track_info_by_id`;
}

export function processEssentialPlaylist() {
    return `${Vue.prototype.$backendUrl}/${PATH_VARIABLES.slp}/${PATH_VARIABLES.spotify}/process_essential_playlist`;
}
