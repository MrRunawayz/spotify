import Axios from 'axios';

export function httpGet(url, config) {
    return http('GET', url, null, config);
}

export function httpPost(url, body, config) {
    return http('POST', url, JSON.stringify(body), config);
}

export function httpPatch(url, body, config) {
    return http('PATCH', url, JSON.stringify(body), config);
}

export function httpDelete(url, config) {
    return http('DELETE', url, null, config);
}

function http(method, url, body, config) {
    return Axios(getHttpConfig(method, url, body, config)).catch(
        error => {
            console.log(error.response);
            console.log(error);

            throw error;
        }
    );
}

function getHttpConfig(method, url, body, config) {
    let httpConfig = {
        method: method,
        url: url,
        data: body ? body : null,
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    };

    return config ? Object.assign(httpConfig, config) : httpConfig;
}
