package com.spotify.myapp.service;

import com.spotify.myapp.domain.Track;
import org.json.JSONException;

import java.net.URISyntaxException;
import java.util.List;

public interface LastFmService {

    List<Track> getLastFmTopTracks() throws URISyntaxException, JSONException;
}
