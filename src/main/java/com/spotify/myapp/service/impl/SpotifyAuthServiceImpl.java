package com.spotify.myapp.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spotify.myapp.domain.AccessTokenResponse;
import com.spotify.myapp.service.SpotifyAuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class SpotifyAuthServiceImpl implements SpotifyAuthService {

    private final RestTemplate restTemplate;

    @Override
    public String getAccessToken(String code, String clientId, String clientSecret) throws IOException {
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("grant_type", "authorization_code");
        requestBody.add("code", code);
        requestBody.add("redirect_uri", "http://localhost:4000/index");
        requestBody.add("client_id", clientId);
        requestBody.add("client_secret", clientSecret);
        URI uri = new DefaultUriBuilderFactory()
                .uriString("https://accounts.spotify.com/api/token")
                .build();
        RequestEntity<MultiValueMap<String, String>> requestEntity = RequestEntity
                .post(uri)
                .accept(MediaType.ALL)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .body(requestBody);


        ResponseEntity<String> response = restTemplate.postForEntity( uri, requestEntity , String.class );
        if (response.getStatusCodeValue() == 200) {
            String responseBody = response.getBody();
            AccessTokenResponse accessTokenResponse = new ObjectMapper().readValue(responseBody, AccessTokenResponse.class);
            return accessTokenResponse.getAccessToken();
        }

        return null;
    }
}
