package com.spotify.myapp.service.impl;

import com.spotify.myapp.domain.Track;
import com.spotify.myapp.service.LastFmService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class LastFmServiceImpl implements LastFmService {

    private final RestTemplate restTemplate;

    @Override
    public List<Track> getLastFmTopTracks() throws URISyntaxException, JSONException {
        List<Track> result = new ArrayList<>();

        int page = 1;
        String responseBody = "";

        do {
            String url = "https://ws.audioscrobbler.com/2.0/?method=user.gettoptracks&user=runawayz&period=overall&limit=1000&page=" + page +"&api_key=1a6d1c0712df35ac411d60fff6ee3d27&format=json";
            URI uri = new URI(url);
            HttpEntity<String> requestEntity = new HttpEntity<>(null);
            ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
            responseBody = response.getBody();

            JSONObject jsonObject = new JSONObject(responseBody);
            JSONObject toptracksJson = jsonObject.getJSONObject("toptracks");
            JSONArray tracksJson = toptracksJson.getJSONArray("track");

            int length = tracksJson.length();
            for(int i=0; i<length; i++) {
                JSONObject jsonObj = tracksJson.getJSONObject(i);

                int playCount = jsonObj.getInt("playcount");
                if (playCount > 5) {
                    String title = jsonObj.getString("name");

                    JSONObject artistJson = jsonObj.getJSONObject("artist");
                    String artist = getArtistName(artistJson.getString("name"));

                    Track track = new Track();
                    track.setArtist(artist);
                    track.setTitle(title);

                    result.add(track);

                    if (i % 100 == 0 || i == length - 1) {
                        log.info("Current Last.Fm tracks result count: {}", result.size());
                    }
                }
            }
            page++;
        } while (!Objects.requireNonNull(responseBody).contains("\"playcount\":\"5\""));


        return result;
    }

    private String getArtistName(String originalName) {
        switch (originalName) {
            case "30 Seconds To Mars":
            case "30 Seconds to Mars":
                return "Thirty Seconds To Mars";
            case "Sound Poets":
                return "The Sound Poets";
            case "Zемфира":
            case "Земфира":
                return "Zemfira";
            case "7Б":
                return "7B";
            case "Animal Джаz":
            case "Animal ДжаZ":
                return "Animal Jazz";
            case "Би-2":
                return "Bi-2";
            case "Братья Грим":
                return "Bratya Grim";
            case "Буерак":
                return "Buerak";
            case "Бумбокс":
                return "Boombox";
            case "Валентин Стрыкало":
                return "Valentin Strykalo";
            case "Гриша Ургант":
                return "Grisha Urgant";
            case "Друга Рiка":
                return "Druha Rika";
            case "Звери":
                return "Zveri";
            case "Коста Лакоста":
                return "Costa Lacoste";
            case "Лолита":
                return "Lolita";
            case "Мачете":
                return "Machete";
            case "Океан Ельзи":
                return "Okean Elzy";
            case "Пасош":
                return "Pasosh";
            case "ПорноФильмы":
                return "Pornofilmy";
            case "Свидание":
                return "Svidaniye";
            case "Сплин":
                return "Splean";
            case "Токио":
                return "Tokio";
            case "Торба-на-Круче":
                return "Torba-na-Kruche";
            case "Христина Соловій":
                return "Khrystyna Soloviy";

            default:
                return originalName;
        }
    }
}
