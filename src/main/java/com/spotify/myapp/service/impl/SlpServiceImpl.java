package com.spotify.myapp.service.impl;

import com.spotify.myapp.domain.Track;
import com.spotify.myapp.service.SlpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class SlpServiceImpl implements SlpService {

    @Override
    public List<Track> getResultTracks(List<Track> spotifyTracks, List<Track> lastFmTopTracks) throws IOException {
        List<Track> result = new ArrayList<>();
        List<Track> notMatchedTracks = new ArrayList<>();

        List<Track> alreadySavedTracks = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("last-fm-top-tracks-04-11-2022.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                Track track = new Track();
                track.setArtist(values[0]);
                track.setTitle(values[1]);
                alreadySavedTracks.add(track);
            }
        }

        List<Track> filteredLastFmTracks = lastFmTopTracks.stream().filter(track -> !alreadySavedTracks.contains(track)).collect(Collectors.toList());

        for (Track lastFmTrack : filteredLastFmTracks) {

            Optional<Track> match = spotifyTracks.stream()
                    .filter(track ->
                            track.getArtist().equalsIgnoreCase(lastFmTrack.getArtist()) &&
                                    track.getTitle().equalsIgnoreCase(lastFmTrack.getTitle())
                    ).findFirst();

            if (match.isPresent()) {
                Track track = match.get();
                result.add(track);
            } else {
                log.info("Track not matched: {} - {}", lastFmTrack.getArtist(), lastFmTrack.getTitle());
                notMatchedTracks.add(lastFmTrack);
            }

        }

        writeToFile(notMatchedTracks);

        return result;
    }

    @Override
    public void saveLastFmTracksToCsv(List<Track> lastFmTopTracks) throws FileNotFoundException {
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formattedString = localDate.format(formatter);

        String fileName = "last-fm-top-tracks-" + formattedString +".csv";
        String headers = "artist,title";
        List<String[]> data = convertToStringArrays(lastFmTopTracks);

        File csvOutputFile = new File(fileName);
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            pw.println(headers);
            data.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        }
    }

    private void writeToFile(List<Track> tracks)
            throws IOException {

        String fileName = "not-matched-tracks" + System.currentTimeMillis() +".csv";
        String headers = "artist,title";
        List<String[]> data = convertToStringArrays(tracks);

        File csvOutputFile = new File(fileName);
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            pw.println(headers);
            data.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        }
    }

    private String convertToCSV(String[] data) {
        return Stream.of(data)
                .map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    private String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

    private List<String[]> convertToStringArrays(List<Track> tracks) {
        List<String[]> dataLines = new ArrayList<>();

        for (Track track : tracks) {
            dataLines.add(new String[]
                {
                    track.getArtist(),
                    track.getTitle()
                }
            );
        }

        return dataLines;

    }
}
