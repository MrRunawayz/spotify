package com.spotify.myapp.service.impl;

import com.google.common.collect.Lists;
import com.spotify.myapp.domain.Track;
import com.spotify.myapp.service.SpotifyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import javax.transaction.Transactional;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class SpotifyServiceImpl implements SpotifyService {

    private final RestTemplate restTemplate;

    @Override
    public List<Track> getLikedSongs(String accessToken) throws URISyntaxException, JSONException, InterruptedException {

//        String stringUri = "https://api.spotify.com/v1/me/tracks";
//        stringUri += "?market=LV&limit=50&offset=0";


        List<Track> result = new ArrayList<>();

        int max = 0;
        int offset = 0;

        // Spotlight
        String playlistId = "5ENbICuY7cMz7un1Dr256c";

        while (offset < max + 1 || max == 0) {
            //String stringUri = "https://api.spotify.com/v1/playlists/" + playlistId + "/tracks?market=LV&offset=" + offset;
            String stringUri = "https://api.spotify.com/v1/me/tracks?market=LV&limit=50&offset=" + offset;
            //String stringUri = "https://api.spotify.com/v1/playlists/" + playlistId +"/tracks?market=LV&limit=50&offset=" + offset;

            URI uri = new URI(stringUri);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + accessToken);
            headers.set("Content-Type", "application/json");

            HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);

            ResponseEntity<String> response = null;

//            try {
//                response = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
//            } catch (HttpClientErrorException e) {
//                if (e.getLocalizedMessage().contains("429")) {
//                    Thread.sleep(30000);
//                    response = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
//                }
//            }


            int count = 0;
            int maxTries = 5;
            while(true) {
                try {
                    response = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
                    break;
                } catch (HttpClientErrorException e) {
                    // handle exception

                    int retrySeconds = Integer.parseInt(Objects.requireNonNull(Objects.requireNonNull(Objects.requireNonNull(e.getResponseHeaders())).get("Retry-After")).get(0));

                    log.info("Retry in... " + retrySeconds + " seconds");

                    Thread.sleep(retrySeconds);
                    if (++count == maxTries) throw e;
                }
            }

//            ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
            if (Objects.requireNonNull(response).getStatusCodeValue() == 200) {
                String responseBody = response.getBody();

                JSONObject jsonObject = new JSONObject(responseBody);

                if (max == 0) {
                    max = jsonObject.getInt("total");
                }

                List<Track> tracksFromJsonPlaylist = getTracksFromJsonPlaylist(jsonObject, accessToken);
                result.addAll(tracksFromJsonPlaylist);
            }
            log.info("Current offset: {}", offset);
            log.info("Current result count: {}", result.size());
            offset = offset + 50;
        }


        return result;
    }

    @Override
    public void saveToPlaylist(List<Track> tracks, String playlistId, String accessToken) {

        if (tracks.isEmpty()) {
            return;
        }

        List<String> tracksUris = tracks.stream().map(Track::getSpotifyTrackUri).collect(Collectors.toList());

        List<List<String>> subLists = Lists.partition(tracksUris, 100);

        for (List<String> list : subLists) {

            StringBuilder stringUri = new StringBuilder("https://api.spotify.com/v1/playlists/" + playlistId + "/tracks");
            stringUri.append("?uris=").append(String.join(",", list));

            MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
//        requestBody.add("range_start", "1");
//        requestBody.add("insert_before", "1");
//        requestBody.add("range_length", "1");
            URI uri = new DefaultUriBuilderFactory()
                    .uriString(String.valueOf(stringUri))
                    .build();
            RequestEntity<MultiValueMap<String, String>> requestEntity = RequestEntity
                    .post(uri)
                    .accept(MediaType.APPLICATION_JSON)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("Authorization", "Bearer " + accessToken)
                    .body(requestBody);

            ResponseEntity<String> response = restTemplate.postForEntity(uri, requestEntity, String.class);
        }

    }

    private List<Track> getTracksFromJsonPlaylist(JSONObject jsonObject, String accessToken) throws JSONException, URISyntaxException, InterruptedException {
        List<Track> result = new ArrayList<>();

        JSONArray tracks = jsonObject.getJSONArray("items");

        if (Objects.isNull(tracks)) {
            return result;
        }

        int length = tracks.length();
        for (int i = 0; i < length; i++) {
            JSONObject jsonObj = tracks.getJSONObject(i);

            JSONObject trackJson = jsonObj.getJSONObject("track");
            String title = trackJson.getString("name");

            JSONObject albumJson = trackJson.getJSONObject("album");
            String album = albumJson.getString("name");

            String uri = "";

            if (trackJson.has("linked_from")) {
                JSONObject linkedJson = trackJson.getJSONObject("linked_from");
                String linkedId = linkedJson.getString("id");

                if (linkedTrackIsSaved(linkedId, accessToken)) {
                    uri = linkedJson.getString("uri");
                } else {
                    uri = trackJson.getString("uri");
                }
            } else {
                uri = trackJson.getString("uri");
            }

            JSONArray artists = trackJson.getJSONArray("artists");
            JSONObject artistJson = artists.getJSONObject(0);
            String artist = artistJson.getString("name");

            Track track = new Track();
            track.setArtist(artist);
            track.setTitle(title);
            track.setAlbum(album);
            track.setSpotifyTrackUri(uri);

            result.add(track);

        }

        return result;
    }

    private boolean linkedTrackIsSaved(String id, String accessToken) throws URISyntaxException, JSONException, InterruptedException {
        URI uri = new URI("https://api.spotify.com/v1/me/tracks/contains" + "?ids=" + id);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + accessToken);
        headers.set("Content-Type", "application/json");

        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);

        try {
            ResponseEntity<String> response = getResponse(uri, requestEntity);
            String responseBody = response.getBody();
            return Objects.requireNonNull(responseBody).contains("true");
        } catch (HttpClientErrorException e) {
            if (e.getLocalizedMessage().contains("429")) {
                Thread.sleep(30000);
                ResponseEntity<String> response = getResponse(uri, requestEntity);
                String responseBody = response.getBody();
                return Objects.requireNonNull(responseBody).contains("true");
            }
        }
        return false;
    }

    private ResponseEntity<String> getResponse(URI uri, HttpEntity<String> requestEntity) {
        return restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
    }

}
