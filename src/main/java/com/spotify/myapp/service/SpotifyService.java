package com.spotify.myapp.service;

import com.spotify.myapp.domain.Track;
import net.minidev.json.parser.ParseException;
import org.json.JSONException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface SpotifyService {

    List<Track> getLikedSongs(String accessToken) throws URISyntaxException, IOException, InterruptedException, ParseException, JSONException;

    void saveToPlaylist(List<Track> tracks, String playlistId, String accessToken);
}
