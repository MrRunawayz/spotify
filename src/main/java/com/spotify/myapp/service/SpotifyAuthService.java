package com.spotify.myapp.service;

import java.io.IOException;
import java.net.URISyntaxException;

public interface SpotifyAuthService {

    String getAccessToken(String code, String clientId, String clientSecret) throws URISyntaxException, IOException, InterruptedException;
}
