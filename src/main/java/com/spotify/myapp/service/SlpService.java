package com.spotify.myapp.service;

import com.spotify.myapp.domain.Track;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface SlpService {

    List<Track> getResultTracks(List<Track> spotifyTracks, List<Track> lastFmTopTracks) throws IOException;

    void saveLastFmTracksToCsv(List<Track> lastFmTopTracks) throws FileNotFoundException;
}
