package com.spotify.myapp.controller;

import com.spotify.myapp.domain.Track;
import com.spotify.myapp.domain.dto.EssentialPlaylistDto;
import com.spotify.myapp.service.LastFmService;
import com.spotify.myapp.service.SlpService;
import com.spotify.myapp.service.SpotifyAuthService;
import com.spotify.myapp.service.SpotifyService;
import com.spotify.myapp.util.JsonUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.parser.ParseException;
import org.json.JSONException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@Transactional
@CrossOrigin(origins = "http://localhost:4000")
@RequestMapping("/spotify")
public class SpotifyController {

    private final SpotifyAuthService spotifyAuthService;
    private final SpotifyService spotifyService;
    private final LastFmService lastFmService;
    private final SlpService slpService;

    public static String CODE;
    public static String ACCESS_TOKEN;
    public static String CLIENT_ID = "2313ba015a6e4262827ec703612e3d6e";
    public static String CLIENT_SECRET = "a85cc550ef094afca9d67beb7a92f794";

    @GetMapping("/process_essential_playlist")
    public String processEssentialPlaylist(@RequestParam String filter) throws URISyntaxException, IOException, InterruptedException, ParseException, JSONException {

        EssentialPlaylistDto dto = getFilterObject(filter);

        CODE = dto.getCode();

        ACCESS_TOKEN = spotifyAuthService.getAccessToken(CODE, CLIENT_ID, CLIENT_SECRET);

        List<Track> lastFmTopTracks = lastFmService.getLastFmTopTracks();
        List<Track> spotifyTracks = spotifyService.getLikedSongs(ACCESS_TOKEN);
        List<Track> resultTracks = slpService.getResultTracks(spotifyTracks, lastFmTopTracks);

        String playlistId = "0e7iUi6zQ0fwAMDFllWgY3";

        spotifyService.saveToPlaylist(resultTracks, playlistId, ACCESS_TOKEN);
        slpService.saveLastFmTracksToCsv(lastFmTopTracks);

        return "Done!";
    }

    private EssentialPlaylistDto getFilterObject(String filter) {

        if (Objects.nonNull(filter) && StringUtils.hasLength(filter)) {
            return JsonUtils.getContentObject(filter, EssentialPlaylistDto.class);
        }

        return null;
    }
}
