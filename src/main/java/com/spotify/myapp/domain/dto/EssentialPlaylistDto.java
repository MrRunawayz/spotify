package com.spotify.myapp.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class EssentialPlaylistDto {

    private Date dateFrom;
    private Date dateTo;
    private String code;
}
