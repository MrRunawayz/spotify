package com.spotify.myapp.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Track {

    private String spotifyTrackUri;
    private String artist;
    private String title;
    private String album;

}
