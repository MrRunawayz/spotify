package com.spotify.myapp.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.ExceptionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;

public class JsonUtils {

    final private static ObjectMapper MAPPER = new ObjectMapper();

    public static <T> T getContentObject(String content, Class<T> valueType) {
        if (StringUtils.isEmpty(content)) return null;
        try {
            return MAPPER.readValue(content, valueType);
        } catch (IOException e) {
            return null;
        }
    }
}
